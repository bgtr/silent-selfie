var http = require("http");
var stream  = require("dronestream");

var server = http.createServer(function(req, res) {
  require("fs").createReadStream(__dirname + "/index.html").pipe(res);
});

stream.listen(server);
server.listen(3000);

client.takeoff();

client
  .after(3000, function() {
    this.up(0.5);
  })
  .after(1000, function() {
  	this.animate('flipRight', 100);
  })
  .after(1000, function() {
    this.up(1);
  })
  .after(1000, function() {
  	this.animate('flipAhead', 100);
  })
  .after(1000, function() {
    this.up(1);
  })
  .after(1000, function() {
  	this.animate('flipLeft', 100);
  })
  .after(500, function() {
    this.stop();
    this.land();
  });
