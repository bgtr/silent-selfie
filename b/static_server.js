/*
var connect = require('connect'),
    serveStatic = require('serve-static');
var app = connect();
app.use(serveStatic(__dirname));
app.listen(8080);
*/
var express = require('express')
var app = express()
var fs = require('fs');
var sys = require('sys');
app.set('views', __dirname + '/views/'); // general config
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/assets'));

var size = 15;
var peekTime = 10;

app.get('/status', function(req, res) {
		res.render('status', {});
})

// respond with "hello world" when a GET request is made to the homepage
app.get('/images', function(req, res) {
		var audios = fs.readdirSync('./assets/audios');
		var peekAudios = getPeekAudios(audios);
		var peek = getPeek(audios);

		var images = fs.readdirSync('./assets/images');
		var peekImages = getPeekImages(images, peek);
		
		res.render('images', {title: 'hello world.', images: peekImages, audios: peekAudios});
})
app.get('/images/:time', function(req, res) {
		var audios = fs.readdirSync('./assets/bk/' + req.params.time + '/audios');
		var peekAudios = getPeekAudios(audios);
		var peek = getPeek(audios);

		var images = fs.readdirSync('./assets/bk/' + req.params.time + '/images');
		var peekImages = getPeekImages(images, peek);
		
		res.render('imagesTime', {time: req.params.time, images: peekImages, audios: peekAudios});
})

function getPeekAudios(audios) {
	var peek = null;
	audios.forEach(function(audio, i){
		if ( audio.match(/first/)) {
			peek = i;
		}
	});
	var start = peek - peekTime;
	return audios.slice(start, start + size);
}

function getPeek(audios) {
	var peek = null;
  for(i = 0; i <= audios.length; i++) {
		var audio = audios[i];
		if ( audio.match(/first/)) {
			var name = audio.replace('_first.wav', '');
			return name;
		}
  }
}

function getPeekImages(images, peek) {
  peekFile = null;
  for(i = 0; i <= images.length; i++) {
		var image = images[i];
    if (image === undefined) {
			continue;
		}
		if ( image.indexOf(peek) != -1) {
			peekFile = i;
		}
	}
	var start = peekFile - peekTime + 3;
	return images.slice(start, start + size);
}

app.listen(8080);
