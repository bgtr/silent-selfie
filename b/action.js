var http = require("http");
var stream  = require("dronestream");
var arDrone = require('ar-drone');
var ejs = require('ejs');
var client  = arDrone.createClient();

var fs = require('fs');
var sys = require('sys');

var lastTime = 0;
var interval = 1000;
var pngStream = client.getPngStream();

var server = http.createServer(function(req, res) {
  if (req.url=='/video.html') {
    require("fs").createReadStream(__dirname + "/video.html").pipe(res);
  } else if (req.url=='/action.html') {
    action();
  } else if (req.url=='/images.html') {
		var files = fs.readdirSync('./images');

		var imagesEjs = fs.readFileSync('./images.ejs', 'utf-8');
		var tmp = ejs.render(imagesEjs, {title: 'hello world.', files: files});
		res.write(tmp);
		res.end();
	}
});

// Server起動
stream.listen(server);
server.listen(3000);

function action() {
  client.takeoff();

  client
/*
    .after(1000, function() {
			this.calibrate(0);
			client._afterOffset = 0;
    })
    .after(1000, function() {
      this.up(1.0);
			client._afterOffset = 0;
    })
*/
    .after(1000, function() {
      this.stop();
			client._afterOffset = 0;
    })
    .after(180000, function() {
      this.land();
			client._afterOffset = 0;
    });

//	setInterval(saveImage, 5000);
  saveImage();
}

var saveImage = function(){
		pngStream
			.on('data', function(pngBuffer) {
				now = (new Date()).getTime();
        // 前回からinterval以上の場合、画像を保存する
				console.log(now);
				console.log(lastTime);
				if (now -lastTime >= interval) {
					lastTime = now;
					fs.writeFile('assets/images/' + now / 1000 + '.png', pngBuffer);
				}
			});
}
